// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {apiKey: "AIzaSyDUcSFNAwQ2HhbQMI_zYxxq4HgbKIEZT6U",
  authDomain: "pitapp-fb022.firebaseapp.com",
  databaseURL: "https://pitapp-fb022.firebaseio.com",
  projectId: "pitapp-fb022",
  storageBucket: "pitapp-fb022.appspot.com",
  messagingSenderId: "376894757224"}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
