import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Medicatie } from '../models/medicatie.interface';
import { FirestoreService } from '../services/data/firestore.service';
import { ActivatedRoute, Router  } from '@angular/router';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  public MedicatieList;
  //public firestoreProvider;
constructor(
  public firestoreService: FirestoreService,
  private router: Router,

) {}
ngOnInit() {
  this.MedicatieList = this.firestoreService.getMedicatieList().valueChanges();
}
}
