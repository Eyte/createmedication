import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Medicatie } from '../../models/medicatie.interface';

export class FirestoreService {
constructor(public firestore: AngularFirestore) {}
@Injectable({  providedIn: 'root'})

createMedication(
  merknaam: string,
  generisch: string,
  dosis: string,
  vorm: string
)
:Promise<void> {
  const id = this.firestore.createId();
  return this.firestore.doc(`MedicatieList/${id}`).set({
      id,
      merknaam,
      generisch,
      dosis,
      vorm,
    });
  }
  getMedicatieList(): AngularFirestoreCollection<Medicatie> {
  return this.firestore.collection(`MedicatieList`);
}
}
