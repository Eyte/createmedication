import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router  } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { FirestoreService } from '../../services/data/firestore.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {
public createMedicationForm: FormGroup;
  constructor(
      public router: Router,
    public loadingCtrl: LoadingController,
  public alertCtrl: AlertController,
  public firestoreService: FirestoreService,
  private formBuilder: FormBuilder) {
  this.createMedicationForm = formBuilder.group({
    merknaam: ['', Validators.required],
    generisch: ['', Validators.required],
    dosis: ['', Validators.required],
    vorm: ['', Validators.required],
  });
}



  ngOnInit() {  }
  async createMedication() {
    const loading = await this.loadingCtrl.create();

    const merknaam = this.createMedicationForm.value.merknaam;
    const generisch = this.createMedicationForm.value.generisch;
    const dosis = this.createMedicationForm.value.dosis;
    const vorm = this.createMedicationForm.value.vorm;

    this.firestoreService
        .createMedication(merknaam, generisch, dosis, vorm)
        .then(
          () => {
            loading.dismiss().then(() => {
              this.router.navigateByUrl('');
            });
          },
          error => {
            console.error(error);
          }
        );
    return await loading.present();
  }
}
